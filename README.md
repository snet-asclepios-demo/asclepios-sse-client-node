# asclepios-sse-client

A javascript client for the ASCLEPIOS symmetric searchable encryption service for Node.js and web browsers. This code is ported directly from the original Django application [ASCLEPIOS SSE Client](https://github.com/UoW-CPC/Asclepios-Client).

## Example Usage

### 1. Install deps

```command
npm install
```

### 2. Set up environment

* Create the file `.env` and add something like the following:

```sh
export TA_URL="http://localhost/ta"
export SSE_URL="http://localhost/sse"
export SALT="ZWdhYndlZmc="
export IV="bGd3YmFnd2c="
export ITER="10000"
export KS="256"
export TS="64"
export HASH_LEN="256"
export CHUNK_SIZE="32768"
export NO_CHUNKS_PER_UPLOAD="30"
export SALT_TA="ZRVja4LFrFY="
export IV_TA="YXp5bWJscWU="
export ITER_TA="10000"
export KS_TA="128"
export TS_TA="64"
export SGX_ENABLE="false"
export CP_ABE_URL="http://localhost:8084"
export AUTH="true"
export DEBUG="true"
export SMALL_FILE="0"

```

```sh
. .env

find . -name '*.js' -type f -exec sed -i "s|ta_url|$TA_URL|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|sse_url|$SSE_URL|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|salt_value|$SALT|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|iv_value|$IV|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|iter_value|$ITER|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|ks_value|$KS|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|ts_value|$TS|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|mode_value|$MODE|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|adata_value|$ADATA|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|adata_len_value|$ADATA_LEN|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|hash_length_value|$HASH_LEN|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|chunk_size_value|$CHUNK_SIZE|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|no_chunks_per_upload_value|$NO_CHUNKS_PER_UPLOAD|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|salt_ta_value|$SALT_TA|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|iv_ta_value|$IV_TA|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|iter_ta_value|$ITER_TA|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|ks_ta_value|$KS_TA|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|ts_ta_value|$TS_TA|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|mode_ta_value|$MODE_TA|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|adata_ta_value|$ADATA_TA|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|adata_len_ta_value|$ADATA_LEN_TA|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|sgx_enable_value|$SGX_ENABLE|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|cp_abe_url|$CP_ABE_URL|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|debug_value|$DEBUG|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|auth_value|$AUTH|g" {} \;
find . -name '*.js' -type f -exec sed -i "s|small_file_value|$SMALL_FILE|g" {} \;
```
## Usage

* see the [ASCLEPIOS SSE Manuals](https://gitlab.com/asclepios-project/ssemanual)


## Other Links
* [ASCLEPIOS SSE Client](https://gitlab.com/asclepios-project/sseclient)
* [ASCLEPIOS SSE Server](https://gitlab.com/asclepios-project/symmetric-searchable-encryption-server)
* [ASCLEPIOS SSE Trusted Authority](https://gitlab.com/asclepios-project/sseta)
* [ASCLEPIOS SSE Deployment](https://gitlab.com/asclepios-project/sse-deployment)
